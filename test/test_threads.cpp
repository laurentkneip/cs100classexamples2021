#include <thread>
#include <chrono>
#include <iostream>

bool running;

void someFunction() {
  
  while(running) {
    std::this_thread::sleep_for( std::chrono::milliseconds(10) );
    std::cout << "Hello\n";
  }

}


int main() {

  running = true;
  std::thread th1( someFunction );

  std::this_thread::sleep_for( std::chrono::milliseconds(5) );
  for ( int i = 0; i < 10000; i++ ) {
    std::this_thread::sleep_for( std::chrono::milliseconds(10) );
    std::cout << "World\n";
  }

  running = false;
  th1.join();
  return 0;
  
}