#include <examples/IntegerWrapper.hpp>
#include <iostream>
#include <ExamplesConfig.h>

int main() {

#ifdef PRINT_WELCOME_MESSAGE
  std::cout << "Welcome to program X version ";
  std::cout << EXAMPLES_V_MAJOR << "." << EXAMPLES_V_MINOR << "\n";
#endif

  IntegerWrapper one(1);
  IntegerWrapper two(2);
  IntegerWrapper six(6);
  IntegerWrapper seven(7);

  return 0;
}