#include <vector>
#include <thread>
#include <iostream>

class Counter {
public:
  Counter() {
    m_value = 0;
  };

  int getValue() {
    return m_value;
  };

  void increment() {
    ++m_value;
  };

private:
  int m_value;
};

void incrementCounterManyTimes( Counter & counter ) {
  for( int i = 0; i < 5000; i++ ) {
    counter.increment();
  }
}

int main() {
  Counter counter;

  std::vector<std::thread> threads;

  for( int i = 0; i < 5; i++ ) {
    threads.push_back( std::thread( incrementCounterManyTimes, std::ref(counter) ) );
  }

  //join all threads
  for( int i = 0; i < 5; i++ ) {
    threads[i].join();
  }

  std::cout << counter.getValue() << "\n";
  return 0;
}