#include <stdlib.h>
#include <math.h>
#include <fstream>

int main( int argc, char * argv[] ) {
  int i;
  double result;

  //make sure we have enough arguments
  if( argc < 2 ) {
    return 1;
  }

  //open the output file
  std::ofstream ss;
  ss.open(argv[1]);

  if(!ss) {
    return 1;
  }

  //create a source file with a table of square roots
  ss << "double sqrtTable[] = {\n";
  for( i = 0; i < 100; i++ ) {
    result = sqrt( static_cast<double>(i) / 10.0 );
    ss << result << ",\n";
  }

  ss << "0};\n";
  ss.close();

  return 0;
}