project(examples)
cmake_minimum_required(VERSION 3.19)

set(EXAMPLES_VERSION_MAJOR 1)
set(EXAMPLES_VERSION_MINOR 0)
add_definitions(-std=c++11)

option( PRINT_WELCOME_MESSAGE "Defines whether or not welcome message is printed" OFF )

configure_file(
  "${PROJECT_SOURCE_DIR}/include/examples/ExamplesConfig.h.in"
  "${PROJECT_BINARY_DIR}/ExamplesConfig.h" )

add_executable( Generator gen/sqrtGenerator.cpp )
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/sqrtTable.inc
  COMMAND Generator ${CMAKE_CURRENT_BINARY_DIR}/sqrtTable.inc
  DEPENDS Generator )
add_custom_target(run ALL DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/sqrtTable.inc )

include_directories( ${PROJECT_BINARY_DIR} )
include_directories( ${PROJECT_SOURCE_DIR}/include )

add_subdirectory( src )
add_subdirectory( test )
