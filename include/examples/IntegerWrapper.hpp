#include <stdlib.h>
#include <iostream>
#include <memory>

class IntegerWrapper {
 public:
  IntegerWrapper( int i );
  virtual ~IntegerWrapper();

  IntegerWrapper operator+( const IntegerWrapper & rhs );
  IntegerWrapper operator-( const IntegerWrapper & rhs );
  IntegerWrapper operator*( const IntegerWrapper & rhs );

  friend std::ostream & operator<<( std::ostream & lhs, const IntegerWrapper & rhs );

 private:
  int _num;
};