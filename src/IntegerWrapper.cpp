#include "examples/IntegerWrapper.hpp"

IntegerWrapper::IntegerWrapper( int i ) : _num(i) {
}

IntegerWrapper::~IntegerWrapper() {}

IntegerWrapper
IntegerWrapper::operator+( const IntegerWrapper & rhs ) {
  return IntegerWrapper(_num + rhs._num);
}

IntegerWrapper
IntegerWrapper::operator-( const IntegerWrapper & rhs ) {
  return IntegerWrapper(_num - rhs._num);
}

IntegerWrapper
IntegerWrapper::operator*( const IntegerWrapper & rhs ) {
  return IntegerWrapper(_num * rhs._num);
}

std::ostream & operator<<( std::ostream & lhs, const IntegerWrapper & rhs ) {
  lhs << rhs._num;
  return lhs;
}